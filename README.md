
  <p align="center">Common library for Nest Applications</p>
    <p align="center">

## Description

Libreria di base per applicazioni Nest JS

## Installation

```bash
$ npm install @findomestic/msa-commons-njs
```

## Functionality

HttpGet -> Wrapper per chiamate GET




## Stay in touch

- Author - [Kamil Myśliwiec](https://kamilmysliwiec.com)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## License

Nest is [MIT licensed](LICENSE).
