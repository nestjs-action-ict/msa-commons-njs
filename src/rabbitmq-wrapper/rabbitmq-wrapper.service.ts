

import {Injectable} from "@nestjs/common";
import {RequestContextService} from "../request-context/request-context.service";
import { RabbitParams } from './rabbitmq-params';
const Broker = require('amqplib');


@Injectable()
export class RabbitWrapperService {

  rabbitParams: RabbitParams;

  constructor(params: RabbitParams) {
   this.rabbitParams = params;
  }

  private async retrieveChannel() {
    const conn = await Broker.connect(`amqp://${this.rabbitParams.user}:${this.rabbitParams.pwd}@${this.rabbitParams.address}/${this.rabbitParams.virtualHost}`);
    return await conn.createChannel();

  }

    private async retrieveChannelForEmit() {
        const channel = await this.retrieveChannel();
        channel.assertQueue(this.rabbitParams.queue);
        return channel;
    }

    private async retrieveChannelForReceive(  )  {
        const channel = await this.retrieveChannel();
        channel.assertExchange('headers-exchange', 'headers', { durable: false });
        channel.assertQueue(this.rabbitParams.queue);
        return channel;
    }

   retrieveHeaders() {
       const requestContextService = new RequestContextService();
       const headers = {
            'x-findomestic-processid': requestContextService.currentContext.pid,
            'x-findomestic-sessionid': requestContextService.currentContext.sid,
            'x-findomestic-conversationid': requestContextService.currentContext.cid,
            'x-findomestic-requestid': requestContextService.currentContext.rid,
            'x-findomestic-sequenceid': Number(requestContextService.currentContext.qid) ,
        }
        return { headers };
    }


    async emit(message){
        const channel = await this.retrieveChannelForEmit();
        channel.sendToQueue(process.env.RABBIT_QUEUE, Buffer.from(JSON.stringify(message)), this.retrieveHeaders())
    }

    async listen( callback ) {
        const channel = await this.retrieveChannelForReceive();
        const q = await channel.assertQueue(process.env.RABBIT_QUEUE, { exclusive: false });
        await channel.consume( q.queue, msg => {
            msg.content = JSON.parse(msg.content.toString());
            callback(msg);
            channel.ack(msg);
        });
    }


}

