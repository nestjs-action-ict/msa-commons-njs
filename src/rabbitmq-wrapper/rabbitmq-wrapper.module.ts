import {Module} from '@nestjs/common';
import {RequestContextModule} from "../request-context/request-context.module";
import {RequestContextService} from "../request-context/request-context.service";
import { RabbitWrapperService } from './rabbitmq-wrapper.service';
import { RabbitParams } from './rabbitmq-params';

@Module( {
  imports: [RequestContextModule, RabbitParams],
  providers: [ RabbitWrapperService, RequestContextService,  RabbitParams],
  exports: [ RabbitWrapperService, RabbitParams ]
})

export class RabbitWrapperModule{}
