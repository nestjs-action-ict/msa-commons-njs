export class RabbitParams {
  user: string;
  pwd:  string;
  virtualHost: string;
  address: string;
  queue: string;
}
