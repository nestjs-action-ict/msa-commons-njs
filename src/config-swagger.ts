import {DocumentBuilder, SwaggerModule} from "@nestjs/swagger";


export async function configSwagger(app, {title, description, version, tag}) {
    const config = new DocumentBuilder()
        .setTitle(title)
        .setDescription(description)
        .setVersion(version)
        .addTag(tag)
        .build();

    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('swagger-ui.html', app, document);

}
