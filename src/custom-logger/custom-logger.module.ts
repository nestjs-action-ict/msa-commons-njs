import { Module } from '@nestjs/common';
import { CustomLogger } from './custom-logger';


@Module( {
  imports: [CustomLoggerModule],
  providers: [CustomLogger, CustomLoggerModule],
  exports: [CustomLogger, CustomLoggerModule]
})

export class CustomLoggerModule{}
