import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';
import { CustomLogger } from './custom-logger';
import { RequestContextService } from '../request-context/request-context.service';
import { tap } from 'rxjs/operators';

@Injectable()
export class LoggingInterceptor implements NestInterceptor{

  incrementQid(context: ExecutionContext) {
    const qid = Number(context.switchToHttp().getRequest().headers['x-findomestic-sequenceid'] || '0') + 1;
    context.switchToHttp().getResponse().setHeader('x-findomestic-sequenceid', qid);
  }

  decrementQid(context: ExecutionContext) {
    const qid = Number(context.switchToHttp().getRequest().headers['x-findomestic-sequenceid'] || '0') - 1;
    context.switchToHttp().getResponse().setHeader('x-findomestic-sequenceid', qid);
  }

  calculateArrows(symbol: string, qid: number) {
    let arrows = symbol + symbol;
    for (let i = 0; i < qid; i++) {
      arrows += symbol;
    }
    return arrows;
  }

  calculateRequestLog(context: RequestContextService): string {
    const qid = Number(context.currentContext.qid);
    return this.calculateArrows('<', qid);
  }

  calculateResponseLog(context: RequestContextService): string {
    const qid = Number(context.currentContext.qid);
    return this.calculateArrows('>', qid);
  }

  defaultHeaderIfNotExists(responseObj, header, value) {
    if (!responseObj.getHeader(header)) {
      responseObj.setHeader(header, value );
    }
  }


  defaultHeaders(context: ExecutionContext) {
    const responseObj = context.switchToHttp().getResponse();
    this.defaultHeaderIfNotExists(responseObj, 'x-findomestic-requestid', '0');
    this.defaultHeaderIfNotExists(responseObj, 'x-findomestic-processid', '0');
    this.defaultHeaderIfNotExists(responseObj, 'x-findomestic-conversationid', '0');
    this.defaultHeaderIfNotExists(responseObj, 'x-findomestic-session', '0');
    this.defaultHeaderIfNotExists(responseObj, 'x-findomestic-sequenceid', '0');
  }

  intercept(context: ExecutionContext, next: CallHandler<any>): Observable<any> | Promise<Observable<any>> {
    const requestContextService = new RequestContextService();
    const logger = new CustomLogger(requestContextService, LoggingInterceptor.name);



    logger.log({
      message: this.calculateRequestLog(requestContextService),
      name: context.getClass().name,
      function: context.getHandler().name,
      //type: context.switchToHttp().contextType,
      host: context.switchToHttp().getRequest().headers.host,
      url: context.switchToHttp().getRequest().url,
      method: context.switchToHttp().getRequest().method
    });

    const now = Date.now();
    return next
      .handle()
      .pipe(
        tap(() => {
          requestContextService.currentContext.elapsed = `${Date.now() - now}ms`
          logger.log({
            message: this.calculateResponseLog(requestContextService),
            elapsed: `${Date.now() - now}ms`,
            name: context.getClass().name,
            function: context.getHandler().name,
            host: context.switchToHttp().getRequest().headers.host,
            url: context.switchToHttp().getRequest().url,
            method: context.switchToHttp().getRequest().method
          })

        }),
      );

    //logger.log('<<');
    //return next.handle().pipe(tap(() => this,logger.log('>>')));
  }

}
