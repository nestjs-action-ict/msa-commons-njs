import axios from 'axios';
import {RequestContextService} from "../request-context/request-context.service";

export async function httpGet(url) {
    const requestContextService = new RequestContextService();
    const headers = {
        'x-findomestic-processid': requestContextService.currentContext.pid,
        'x-findomestic-sessionid': requestContextService.currentContext.sid,
        'x-findomestic-conversationid': requestContextService.currentContext.cid,
        'x-findomestic-requestid': requestContextService.currentContext.rid,
        'x-findomestic-sequenceid': Number(requestContextService.currentContext.qid) + 1,
    }
    return axios.get(url, {headers})
}
