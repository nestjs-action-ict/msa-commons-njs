

import {AxiosResponse} from "axios";
import {HttpService, Injectable} from "@nestjs/common";
import {Observable} from "rxjs";
import {RequestContextService} from "../request-context/request-context.service";

@Injectable()
export class HttpWrapperService {
    constructor(private httpService: HttpService) {}

    get(url): Observable<AxiosResponse<any>> {
        const requestContextService = new RequestContextService();
        const headers =  {
            'x-findomestic-processid': requestContextService.currentContext.pid,
            'x-findomestic-sessionid': requestContextService.currentContext.sid,
            'x-findomestic-conversationid': requestContextService.currentContext.cid,
            'x-findomestic-requestid': requestContextService.currentContext.rid,
            'x-findomestic-sequenceid': Number(requestContextService.currentContext.qid) + 1,
        }
        return this.httpService.get(url, {headers});
    }
}

