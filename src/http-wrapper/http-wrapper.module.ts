import {HttpModule, Module} from '@nestjs/common';
import {RequestContextModule} from "../request-context/request-context.module";
import {HttpWrapperService} from "./http-wrapper.service";
import {RequestContextService} from "../request-context/request-context.service";


@Module( {
  imports: [HttpModule, RequestContextModule],
  providers: [ HttpWrapperService, RequestContextService],
  exports: [ HttpWrapperService ]
})

export class HttpWrapperModule{}
